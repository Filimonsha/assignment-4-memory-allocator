#ifndef ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

#define HEAP_CAPACITY 8192
#define QUERY_SIZE 1024

#define TEST_HEAP_INIT "Heap initialized: "
#define TEST_HEAP_DEINIT "Heap deinitialized: "
#define TEST_ONE "Test one completed: "
#define TEST_TWO "Test two completed: "
#define TEST_THREE "Test three completed: "
#define TEST_FOUR "Test four completed: "
#define TEST_FIVE "Test five completed: "

inline void deinit_heap(void * addr, size_t size) {
    size_t mem_size = size_from_capacity((block_capacity) {size}).bytes;
    munmap(addr, mem_size);
}

inline struct block_header* count_block_start(void* addr) {
    return (struct block_header*) ((uint8_t*) addr - offsetof(struct block_header, contents));
}

void test_one(FILE* f);
void test_two(FILE* f);
void test_three(FILE* f);
void test_four(FILE* f);
void test_five(FILE* f);
#endif //ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H
