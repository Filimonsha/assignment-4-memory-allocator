#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static bool block_exists(struct block_header* block) {return block != NULL; }
static bool next_block_exists(struct block_header* block) {return block_exists(block) && block->next;}
static size_t pages_count   ( size_t mem ) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t round_pages   ( size_t mem ) { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    block_size init_size = size_from_capacity((block_capacity) {query});
    size_t region_size = region_actual_size(init_size.bytes);

    void * region_address = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
    if (region_address == MAP_FAILED) {
        region_address = map_pages(addr, region_size, 0);
        if (region_address == MAP_FAILED) {
            return REGION_INVALID;
        }
    }

    block_size block_size = {region_size};
    block_init(region_address, block_size, NULL);
    struct region region = {region_address, region_size, region_address == addr };
    return region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (block != NULL && block_splittable(block, query)) {
        void* addr = block -> contents + query;
        size_t second_block_size_bytes = block -> capacity.bytes - query;
        block_init( addr,
                   (block_size) {second_block_size_bytes},
                   block -> next );
        block -> next = addr;
        block -> capacity.bytes = query;
        return true;
    }
    else {
        return false;
    }
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd ) {
    return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static inline bool block_exists_and_mergable(struct block_header* block) {
    return next_block_exists(block) && mergeable(block, block -> next);
}

static bool try_merge_with_next( struct block_header* block ) {
    if (block_exists_and_mergable(block)) {
        struct block_header* second_block_header = block -> next;
        block_size second_block_size = size_from_capacity(second_block_header -> capacity);
        block -> next = second_block_header -> next;
        block -> capacity = (block_capacity) {block -> capacity.bytes + second_block_size.bytes};
        return true;
    }
    else {
        return false;
    }
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

static inline bool block_satisfies_query(struct block_header* block, size_t query) {
    return block -> is_free && block_is_big_enough(query, block);
}


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    sz = size_max(sz, BLOCK_MIN_CAPACITY);
    if (!block_exists(block)) {
        return (struct block_search_result) {BSR_CORRUPTED, block};
    }
    else {
        while (block_exists(block)) {
            while (try_merge_with_next(block));
            if (block_satisfies_query(block, sz)) {
                return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, block };
            }
            else if (!next_block_exists(block)) {
                break;
            }
            block = block -> next;
        }
        return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, block};
    }
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result search_result = find_good_or_last(block, query);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(search_result.block, query);
        search_result.block->is_free = false;
    }
    return search_result;
}



static struct block_header* grow_heap( struct block_header* restrict block, size_t query ) {
    if (block == NULL) return NULL;
    void* next_block_address = block_after(block);
    struct region new_region = alloc_region(next_block_address, query);
    if (!region_is_invalid(&new_region)) {
        block -> next = new_region.addr;
        bool is_merged = try_merge_with_next(block);
        if (is_merged) return block;
        else return new_region.addr;
    }
    else return NULL;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result memaloc_result = try_memalloc_existing(query, heap_start);
    switch(memaloc_result.type) {
        case BSR_FOUND_GOOD_BLOCK:
            return memaloc_result.block;
            break;
        case BSR_CORRUPTED:
            return NULL;
            break;
        case BSR_REACHED_END_NOT_FOUND:
            memaloc_result.block = grow_heap(memaloc_result.block, query);
            memaloc_result = try_memalloc_existing(query, memaloc_result.block);
            return memaloc_result.block;
            break;
    }
    return NULL;
}

void* _malloc( size_t query ) {
    struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  do {} while (try_merge_with_next(header));
}
