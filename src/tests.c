#include "tests.h"

extern void deinit_heap(void * addr, size_t size);
extern struct block_header* count_block_start(void* addr);

void test_one(FILE* f) {
    void* heap = heap_init(HEAP_CAPACITY);
    char* result = heap == NULL ? "false" : "true";
    fprintf(f, "%s%s\n", TEST_HEAP_INIT, result);
    if (heap != NULL) {
        debug_heap(f, heap);

        void *mem = _malloc(QUERY_SIZE);
        if (mem != NULL) {
            debug_heap(f, heap);

            _free(mem);
            debug_heap(f, heap);

            deinit_heap(heap, HEAP_CAPACITY);
            debug_heap(f, heap);
            fprintf(f, "%s%s\n", TEST_ONE, "true");
        }
        else {
            fprintf(f, "%s%s\n", TEST_ONE, "false");
        }
    }
    else {
        fprintf(f, "%s%s\n", TEST_HEAP_INIT, "false");
        fprintf(f, "%s%s\n", TEST_TWO, "false");
    }
}

void test_two(FILE* f) {
    void* heap = heap_init(HEAP_CAPACITY);
    if (heap != NULL) {
        fprintf(f, "%s%s\n", TEST_HEAP_INIT, "true");
        debug_heap(f, heap);

        void* mem1 = _malloc(QUERY_SIZE);
        void* mem2 = _malloc(QUERY_SIZE);
        debug_heap(f, heap);
        if (mem1 && mem2) {
            _free(mem1);
            debug_heap(f, heap);

            deinit_heap(heap, HEAP_CAPACITY);
            fprintf(f, "%s%s\n", TEST_TWO, "true");
        } else
            fprintf(f, "%s%s\n", TEST_TWO, "false");
    }
    else {
        fprintf(f, "%s%s\n", TEST_HEAP_INIT, "false");
        fprintf(f, "%s%s\n", TEST_TWO, "false");
    }
    deinit_heap(heap, HEAP_CAPACITY);
}

void test_three(FILE* f) {
    void* heap = heap_init(HEAP_CAPACITY);
    if (heap != NULL) {
        fprintf(f, "%s%s\n", TEST_HEAP_INIT, "true");
        debug_heap(f, heap);

        void* mem1 = _malloc(QUERY_SIZE);
        void* mem2 = _malloc(QUERY_SIZE);
        void* mem3 = _malloc(QUERY_SIZE);
        void* mem4 = _malloc(QUERY_SIZE);
        void* mem5 = _malloc(QUERY_SIZE);
        debug_heap(f, heap);
        if (mem1 && mem2 && mem3 && mem4 && mem5) {
            _free(mem1);
            debug_heap(f, heap);

            _free(mem4);
            deinit_heap(heap, HEAP_CAPACITY);

            fprintf(f, "%s%s\n", TEST_THREE, "true");
        } else
            fprintf(f, "%s%s\n", TEST_THREE, "false");
    }
    else {
        fprintf(f, "%s%s\n", TEST_HEAP_INIT, "false");
        fprintf(f, "%s%s\n", TEST_TWO, "false");
    }
    deinit_heap(heap, HEAP_CAPACITY);
}

void test_four(FILE* f) {
    void* heap = heap_init(HEAP_CAPACITY);
    if (heap != NULL) {
        bool heap_is_ok = true;
        size_t i;
        void* allocated_mem[HEAP_CAPACITY / QUERY_SIZE];
        size_t mem_array_size = sizeof(allocated_mem) / sizeof(allocated_mem[0]);
        struct block_header* mem_headers[mem_array_size];
        struct block_header* last_header;

        fprintf(f, "%s%s\n", TEST_HEAP_INIT, "true");
        debug_heap(f, heap);

        for (i = 0; i < mem_array_size; i++) {
            allocated_mem[i] = _malloc(QUERY_SIZE);
            if (allocated_mem[i] == NULL) {
                fprintf(f, "%s%s\n", TEST_FOUR, "false");
                deinit_heap(heap, HEAP_CAPACITY);
                return;
            }
            mem_headers[i] = count_block_start(allocated_mem[i]);
        }
        debug_heap(f, heap);

        for (i = 0; i < mem_array_size; i++) {
            heap_is_ok = heap_is_ok && mem_headers[i] -> capacity.bytes == QUERY_SIZE && mem_headers[i] -> contents == allocated_mem[i];
            if (mem_headers[i] -> next == NULL) last_header = mem_headers[i];
        }

        if (heap_is_ok) {
            void* new_alloc = _malloc(QUERY_SIZE);
            struct block_header* new_block_header = count_block_start(new_alloc);
            void* expected_addr = (void*) (new_block_header -> contents + new_block_header -> capacity.bytes);
            if (new_alloc != NULL && expected_addr != last_header) {
                fprintf(f, "%s%s\n", TEST_TWO, "true");
            }
            else fprintf(f, "%s%s\n", TEST_TWO, "false");
        } else fprintf(f, "%s%s\n", TEST_TWO, "false");

    }
    else {
        fprintf(f, "%s%s\n", TEST_HEAP_INIT, "false");
        fprintf(f, "%s%s\n", TEST_TWO, "false");
    }
    deinit_heap(heap, HEAP_CAPACITY);
}

void test_five(FILE* f) {
    void *heap_one = heap_init(HEAP_CAPACITY);
    if (heap_one != NULL) {
        void *mem1 = _malloc(HEAP_CAPACITY);
        debug_heap(f, heap_one);

        void *heap_two = heap_init(HEAP_CAPACITY);
        if (heap_two != NULL) {
            void *mem2 = _malloc(QUERY_SIZE);
            debug_heap(f, heap_one);
            debug_heap(f, heap_two);

            struct block_header *block1 = count_block_start(mem1);
            struct block_header *block2 = count_block_start(mem2);
            if (block1->next != block2) {
                fprintf(f, "%s%s\n", TEST_FIVE, "true");
                deinit_heap(heap_one, HEAP_CAPACITY);
                deinit_heap(heap_two, HEAP_CAPACITY);
                return;
            }
        } else fprintf(f, "%s%s\n", TEST_FIVE, "false");
    } else fprintf(f, "%s%s\n", TEST_HEAP_INIT, "false");
}
